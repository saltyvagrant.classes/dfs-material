import pytest

@pytest.mark.parametrize("name,version", [
    ("nginx", "1.14"),
])
def test_packages(host, name, version):
    pkg = host.package(name)
    assert pkg.is_installed
    assert pkg.version.startswith(version)


def test_service(host):
    service = host.service("nginx")
    assert service.is_running
    assert service.is_enabled
