{% set std_tools = salt['pillar.get']('std_tools',{}) %}

install_std_tools:
  pkg.installed:
    - pkgs:
{%- for pkg in std_tools.packages %}
      - {{ pkg }}
{%- endfor %}
